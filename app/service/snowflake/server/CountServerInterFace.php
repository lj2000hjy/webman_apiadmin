<?php
namespace app\service\snowflake\server;

interface CountServerInterFace
{
    public function getSequenceId($key);
}