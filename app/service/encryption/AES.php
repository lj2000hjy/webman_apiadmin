<?php


namespace app\service\encryption;


/**
 * openssl AES加密解密
 *
 * @author Hawind <hawind@qq.com>
 *
 * @version 1.0
 */
class AES
{

    /**
     * 加密用的key值生成
     * @return false|string
     */
    public static function getKey()
    {
        $data = md5(date('Ymd'));
        return substr($data,0,16);
    }

    /**
     * 加密用的Iv偏移量
     * @return false|string
     */
    public static function getIv()
    {
        $data = md5(self::getKey());
        return substr($data,0,16);
    }

    /**
     * openssl aes 加密
     * @param $data
     * @param string $key
     * @param int $options
     * @return false|string
     */
    public static function crypto_encrypt($data, $key="", $options = OPENSSL_RAW_DATA)
    {
        if(empty($key)){
            $key = md5(date('Ymd'));
        }
        $iv = substr($key, 0, -16);
        $encrypted = openssl_encrypt($data, 'aes-256-cbc', $key, $options, $iv);
        return $encrypted;
    }

    /**
     * openssl aes 解密
     */
    public static function crypto_decrypt($data, $key, $options = OPENSSL_RAW_DATA)
    {
        $iv = substr($key, 0, -16);
        return openssl_decrypt($data, 'aes-256-cbc', $key, $options, $iv);
    }

    /**
     * 加密.
     *
     * @param mixed $contents 要加密的内容
     * @param string $encryptKey 加密的Key，长度为14，24，32
     *
     * @return string 已加密的内容
     */
    public static function encrypt($data, $key)
    {
        $iv = openssl_random_pseudo_bytes(16);
        $encrypted = [
            base64_encode($iv),
            openssl_encrypt($data, 'aes-256-cbc', $key, 0, $iv)
        ];
        return base64_encode(json_encode($encrypted));
    }

    /**
     * [encrypt aes加密]
     * @param    [type]                   $input [要加密的数据]
     * @param    [type]                   $key   [加密key]
     * @return   [type]                          [加密后的数据]
     */
    public static function ecb_encrypt($input, $key)
    {
        $key = self::_sha1prng($key);
        $iv = '';
        $data = openssl_encrypt($input, 'AES-128-ECB', $key, OPENSSL_RAW_DATA, $iv);
        $data = base64_encode($data);
        return $data;
    }

    /**
     * [decrypt aes解密]
     * @param    [type]                   $sStr [要解密的数据]
     * @param    [type]                   $sKey [加密key]
     * @return   [type]                         [解密后的数据]
     */
    public static function ecb_decrypt($sStr, $sKey)
    {
        $sKey = self::_sha1prng($sKey);
        $iv = '';
        $decrypted = openssl_decrypt(base64_decode($sStr), 'AES-128-ECB', $sKey, OPENSSL_RAW_DATA, $iv);
        return $decrypted;
    }

    /**
     * SHA1PRNG算法
     * @param  [type] $key [description]
     * @return [type]      [description]
     */
    private static function _sha1prng($key)
    {
        return substr(openssl_digest(openssl_digest($key, 'sha1', true), 'sha1', true), 0, 16);
    }

    /**
     * 解密.
     *
     * @param string $data 已加密的内容
     * @param string $key 解密Key
     *
     * @return string 已解密的内容
     */
    public static function decrypt($data, $key)
    {
        $encrypt = json_decode(base64_decode($data), true);
        $iv = base64_decode($encrypt[0]);
        $decrypted = openssl_decrypt($encrypt[1], 'aes-256-cbc', $key, 0, $iv);
        return $decrypted;
    }
}
