<?php

namespace app\middleware;

use app\service\statistics\Clients\StatisticClient;
use Webman\MiddlewareInterface;
use Webman\Http\Response;
use Webman\Http\Request;

class Statistic implements MiddlewareInterface
{
    public function process(Request $request, callable $next): Response
    {
        $ip         = $request->getRealIp($safe_mode = true);
        $controller = $request->controller;
        $action     = $request->action;
        $transfer   = $controller . '::' . $action;

        // 开始计时
        $unique = StatisticClient::tick('project', $ip, $transfer);

        $response = $next($request);

        $code    = $response->getStatusCode();
        $success = $code < 400;
        $details = [
            'ip'              => $request->getRealIp($safe_mode = true) ?? '',   // 请求客户端IP
            'url'             => $request->fullUrl() ?? '',                      // 请求URL
            'method'          => $request->method() ?? '',                       // 请求方法
            'request_param'   => $request->all() ?? [],                          // 请求参数
            'request_header'  => $request->header() ?? [],                       // 请求头
            'cookie'          => $request->cookie() ?? [],                       // 请求cookie
            'session'         => $request->session()->all() ?? [],               // 请求session
            'response_code'   => $response->getStatusCode() ?? '',               // 响应码
            'response_header' => $response->getHeaders() ?? [],                  // 响应头
        ];
        // 数据上报
        StatisticClient::report($unique, 'project', $ip, $transfer, $success, $code, json_encode($details, 320));

        return $response;
    }
}
