<?php


namespace app\middleware;

use app\util\ReturnCode;
use Webman\Http\Request;
use Webman\Http\Response;
use Webman\MiddlewareInterface;

class ApiPermission implements MiddlewareInterface {

    /**
     * 校验当前App是否有请求当前接口的权限
     * @param Request $request
     * @param callable $next
     * @return Response
     */
    public function process(Request $request, callable $next): Response{
        $appInfo = $request->APP_CONF_DETAIL;
        $apiInfo = $request->API_CONF_DETAIL;
        $allRules = explode(',', $appInfo['app_api']);
        if (!in_array($apiInfo['hash'], $allRules)) {
            return json([
                'code' => ReturnCode::INVALID,
                'msg'  => '非常抱歉，您没有权限这么做！',
                'data' => []
            ]);
        }

        return $next($request);
    }
}
