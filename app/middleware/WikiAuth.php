<?php


namespace app\middleware;

use app\util\ReturnCode;
use Webman\Http\Request;
use Webman\Http\Response;
use Webman\MiddlewareInterface;

class WikiAuth implements MiddlewareInterface {

    /**
     * ApiAuth鉴权
     * @param \think\facade\Request $request
     * @param \Closure $next
     * @return mixed|\think\response\Json
     * @author zhaoxiang <zhaoxiang051405@gmail.com>
     */
    public function process(Request $request, callable $next): Response{
        $ApiAuth = $request->header('Api-Auth', '');
        if ($ApiAuth) {
            $userInfo = cache('Login' . $ApiAuth);
            if (!$userInfo) {
                $userInfo = cache('WikiLogin' . $ApiAuth);
            } else {
                $userInfo = json_decode($userInfo, true);
                $userInfo['app_id'] = -1;
            }
            if (!$userInfo || !isset($userInfo['id'])) {
                return json([
                    'code' => ReturnCode::AUTH_ERROR,
                    'msg'  => 'ApiAuth不匹配',
                    'data' => []
                ]);
            } else {
                $request->API_WIKI_USER_INFO = $userInfo;
            }

            return $next($request);
        } else {
            return json([
                'code' => ReturnCode::AUTH_ERROR,
                'msg'  => '缺少ApiAuth',
                'data' => []
            ]);
        }
    }
}
