<?php


namespace app\command;

use GatewayWorker\BusinessWorker;
use GatewayWorker\Gateway;
use GatewayWorker\Register;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Workerman\Worker;
use Symfony\Component\Console\Input\InputArgument;

class GatewayCommand extends \Symfony\Component\Console\Command\Command
{

    protected static $defaultName = 'config:gateway';
    protected static $defaultDescription = 'gateway服务配置';

    protected function configure()
    {
        $this
        // 命令的名称 ("php console_command" 后面的部分)
        //->setName('model:create')
        // 运行 "php console_command list" 时的简短描述
        ->setDescription('Create new model')
        // 运行命令时使用 "--help" 选项时的完整命令描述
        ->setHelp('This command allow you to create models...')
        // 配置一个参数
        ->addArgument('name', InputArgument::REQUIRED, 'what\'s model you want to create ?');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        global $argv;
//        $argv[2] = 'start';
        $argv[3] = '-d';

        $output->writeln('gateway服务配置信息如下：');

        $this->startGateWay();
        $this->startBusinessWorker();
        $this->startRegister();
        Worker::runAll();
//        $table = new Table($output);
//        $table->setHeaders($headers);
//        $table->setRows($row);
//        $table->render();
        return self::SUCCESS;
    }

    private function startBusinessWorker()
    {
        $worker = new BusinessWorker();
        $worker->name = 'BusinessWorker';
        $worker->count = 1;
        $worker->registerAddress = '127.0.0.1:1237';
        $worker->eventHandler = app_path() . "/service/Events.php";
    }

    private function startGateWay()
    {
        $gateway = new Gateway("websocket://0.0.0.0:2347");
        $gateway->name = 'Gateway';
        $gateway->count = 4;
        $gateway->lanIp = '127.0.0.1';
        $gateway->startPort = 40001;
        $gateway->pingInterval = 30;
        $gateway->pingNotResponseLimit = 0;
        $gateway->pingData = '{"type":"ping"}';
        $gateway->registerAddress = '127.0.0.1:1237';//正式 1237  测试 1236

    }

    private function startRegister()
    {
        new Register('text://0.0.0.0:1237');
    }
}
