<?php


namespace app\command;


use PHPSocketIO\SocketIO;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Workerman\Worker;

class SocketIOCommand extends \Symfony\Component\Console\Command\Command
{

    /**
     * 文档地址: https://www.workerman.net/phpsocket_io
     * 加入分组: $socket->join('group name');
     * 离开分组: $socket->leave('group name');
     * 向当前客户端发送事件: $socket->emit('event name', $data);
     * 向所有客户端发送事件: $io->emit('event name', $data);
     * 向所有客户端发送事件，但不包括当前连接: $socket->broadcast->emit('event name', $data);
     * 向某个分组的所有客户端发送事件: $io->to('group name')->emit('event name', $data);
     * 关闭链接: $socket->disconnect();
     * 限制连接域名: $io->origins('http://example.com:8080'); $io->origins('http://workerman.net http://www.workerman.net');
     *
     * @var string
     */
    protected static $defaultName = 'config:SocketIO';
    protected static $defaultDescription = 'SocketIO服务配置';

    protected function configure()
    {
        $this
            // 命令的名称 ("php console_command" 后面的部分)
            //->setName('model:create')
            // 运行 "php console_command list" 时的简短描述
            ->setDescription('Create new model')
            // 运行命令时使用 "--help" 选项时的完整命令描述
            ->setHelp('This command allow you to create models...')
            // 配置一个参数
            ->addArgument('name', InputArgument::REQUIRED, 'what\'s model you want to create ?');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        global $argv;
//        $argv[2] = 'start';
        $argv[3] = '-d';

        $output->writeln('SocketIO服务配置信息如下：');

        $io = new SocketIO(3120);
        // 当有客户端连接时
        $io->on('connection', function($socket)use($io){
            // 定义chat message事件回调函数
            $socket->on('chat message', function($msg)use($io){
                // 触发所有客户端定义的chat message from server事件
                $io->emit('chat message from server', $msg);
            });
        });
        // 监听一个http端口，通过http协议访问这个端口可以向所有客户端推送数据(url类似http://ip:9191?msg=xxxx)
        $io->on('workerStart', function()use($io) {
            $inner_http_worker = new Worker('http://0.0.0.0:9191');
            $inner_http_worker->onMessage = function($http_connection, $data)use($io){
                if(!isset($_GET['msg'])) {
                    return $http_connection->send('fail, $_GET["msg"] not found');
                }
                $io->emit('chat message', $_GET['msg']);
                $http_connection->send('ok');
            };
            $inner_http_worker->listen();
        });

        Worker::runAll();
        return self::SUCCESS;
    }
}
