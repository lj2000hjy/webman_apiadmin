<?php
declare (strict_types=1);
/**
 * 应用管理
 * @since   2021-11-17
 * @author  bubaishaolong <584887013@qq.com>
 */

namespace app\admin\controller;

use app\model\AdminApp;
use app\model\AdminList;
use app\model\AdminGroup;
use app\util\ReturnCode;
use app\util\Strs;
use app\util\Tools;
use support\Response;

class App extends Base {

    /**
     * 获取应用列表
     * @return Response
     * @throws \think\db\exception\DbException
     */
    public function index(): Response {
        $request = request();
        $limit =$request->get('size', config('apiwebman.ADMIN_LIST_DEFAULT'));
        $start =$request->get('page', 1);
        $keywords =$request->get('keywords', '');
        $type =$request->get('type', '');
        $status =$request->get('status', '');
        $obj = new AdminApp();
        if (strlen($status)) {
            $obj = $obj->where('app_status', $status);
        }
        if ($type) {
            switch ($type) {
                case 1:
                    $obj = $obj->where('app_id', $keywords);
                    break;
                case 2:
                    $obj = $obj->whereLike('app_name', "%{$keywords}%");
                    break;
            }
        }
        $listObj = $obj->order('app_add_time', 'DESC')->paginate(['page' => $start, 'list_rows' => $limit])->toArray();

        return $this->buildSuccess([
            'list'  => $listObj['data'],
            'count' => $listObj['total']
        ]);
    }

    /**
     * 获取AppId,AppSecret,接口列表,应用接口权限细节
     * @return Response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function getAppInfo(): Response {
        $request = request();
        $apiArr = (new AdminList())->select();
        foreach ($apiArr as $api) {
            $res['apiList'][$api['group_hash']][] = $api;
        }
        $groupArr = (new AdminGroup())->select();
        $groupArr = Tools::buildArrFromObj($groupArr);
        $res['groupInfo'] = array_column($groupArr, 'name', 'hash');
        $id =$request->get('id', 0);
        if ($id) {
            $appInfo = (new AdminApp())->where('id', $id)->find()->toArray();
            $res['app_detail'] = json_decode($appInfo['app_api_show'], true);
        } else {
            $res['app_id'] = mt_rand(1, 9) . Strs::randString(7, 1);
            $res['app_secret'] = Strs::randString(32);
        }

        return $this->buildSuccess($res);
    }

    /**
     * 刷新APPSecret
     * @return Response
     * @author bubaishaolong <584887013@qq.com>
     */
    public function refreshAppSecret(): Response {
        $data['app_secret'] = Strs::randString(32);

        return $this->buildSuccess($data);
    }

    /**
     * 新增应用
     * @return Response
     * @author bubaishaolong <584887013@qq.com>
     */
    public function add(): Response {
        $request = request();
        $postData =$request->post();
        $data = [
            'app_id'       => $postData['app_id'],
            'app_secret'   => $postData['app_secret'],
            'app_name'     => $postData['app_name'],
            'app_info'     => $postData['app_info'],
            'app_group'    => $postData['app_group'],
            'app_add_time' => time(),
            'app_api'      => '',
            'app_api_show' => ''
        ];
        if (isset($postData['app_api']) && $postData['app_api']) {
            $appApi = [];
            $data['app_api_show'] = json_encode($postData['app_api']);
            foreach ($postData['app_api'] as $value) {
                $appApi = array_merge($appApi, $value);
            }
            $data['app_api'] = implode(',', $appApi);
        }
        $res = AdminApp::create($data);
        if ($res === false) {
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR);
        }

        return $this->buildSuccess();
    }

    /**
     * 应用状态编辑
     * @return Response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function changeStatus(): Response {
        $request = request();
        $id =$request->get('id');
        $status =$request->get('status');
        $res = AdminApp::update([
            'id'         => $id,
            'app_status' => $status
        ]);
        if ($res === false) {
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR);
        }
        $appInfo = (new AdminApp())->where('id', $id)->find();
        cache('AccessTokenEasy' . $appInfo['app_secret'], null);
        if ($oldWiki = cache('WikiLogin' . $id)) {
            cache('WikiLogin' . $oldWiki, null);
        }

        return $this->buildSuccess();
    }

    /**
     * 编辑应用
     * @return Response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function edit(): Response {
        $request = request();
        $postData =$request->post();
        $data = [
            'app_secret'   => $postData['app_secret'],
            'app_name'     => $postData['app_name'],
            'app_info'     => $postData['app_info'],
            'app_group'    => $postData['app_group'],
            'app_api'      => '',
            'app_api_show' => ''
        ];
        if (isset($postData['app_api']) && $postData['app_api']) {
            $appApi = [];
            $data['app_api_show'] = json_encode($postData['app_api']);
            foreach ($postData['app_api'] as $value) {
                $appApi = array_merge($appApi, $value);
            }
            $data['app_api'] = implode(',', $appApi);
        }
        $res = AdminApp::update($data, ['id' => $postData['id']]);
        if ($res === false) {
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR);
        }
        $appInfo = (new AdminApp())->where('id', $postData['id'])->find();
        cache('AccessTokenEasy' . $appInfo['app_secret'], null);
        if ($oldWiki = cache('WikiLogin' . $postData['id'])) {
            cache('WikiLogin' . $oldWiki, null);
        }

        return $this->buildSuccess();

    }

    /**
     * 删除应用
     * @return Response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function del(): Response {
        $request = request();
        $id =$request->get('id');
        if (!$id) {
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS, '缺少必要参数');
        }
        $appInfo = (new AdminApp())->where('id', $id)->find();
        cache('AccessTokenEasy' . $appInfo['app_secret'], null);

        AdminApp::destroy($id);
        if ($oldWiki = cache('WikiLogin' . $id)) {
            cache('WikiLogin' . $oldWiki, null);
        }

        return $this->buildSuccess();
    }
}
