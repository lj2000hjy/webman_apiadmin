<?php
declare (strict_types=1);

namespace app\admin\controller;

use app\util\ReturnCode;
use support\Response;

class Miss extends Base {

    public function index(): Response {
        $request = request();
        if ($request->method() == 'OPTIONS') {
            return $this->buildSuccess();
        } else {
            return $this->buildFailed(ReturnCode::INVALID, '接口地址异常');
        }
    }
}
