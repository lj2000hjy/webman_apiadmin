<?php
declare (strict_types=1);
/**
 * 后台操作日志管理
 * @since   2018-02-06
 * @author  bubaishaolong <584887013@qq.com>
 */

namespace app\admin\controller;

use app\model\AdminUserAction;
use app\util\ReturnCode;
use support\Response;

class Log extends Base {

    /**
     * 获取操作日志列表
     * @return Response
     * @throws \think\db\exception\DbException
     * @author bubaishaolong <584887013@qq.com>
     */
    public function index(): Response  {
        $limit =request()->get('size', config('apiwebman.ADMIN_LIST_DEFAULT'));
        $start =request()->get('page', 1);
        $type =request()->get('type', '');
        $keywords =request()->get('keywords', '');
        $offset = $start*$limit;
        $obj = new AdminUserAction();
        $id =$obj->get_table_number($offset);
        $obj = $obj::suffix($id);
        if ($type) {
            switch ($type) {
                case 1:
                    $obj = $obj->whereLike('url', "%{$keywords}%");
                    break;
                case 2:
                    $obj = $obj->whereLike('nickname', "%{$keywords}%");
                    break;
                case 3:
                    $obj = $obj->where('uid', $keywords);
                    break;
            }
        }
        $listObj = $obj->order('add_time', 'DESC')->paginate(['page' => $start, 'list_rows' => $limit])->toArray();

        return $this->buildSuccess([
            'list'  => $listObj['data'],
            'count' => $listObj['total']
        ]);
    }

    /**
     * 删除日志
     * @return Response
     * @author bubaishaolong <584887013@qq.com>
     */
    public function del(): Response  {
        $id =request()->get('id');
        if (!$id) {
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS, '缺少必要参数');
        }
        $obj = new AdminUserAction();
        $id_suffix =$obj->get_table_number($id);
        AdminUserAction::suffix($id_suffix)->destroy($id);
        return $this->buildSuccess();
    }
}
