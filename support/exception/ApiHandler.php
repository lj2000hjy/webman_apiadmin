<?php


namespace support\exception;


use app\util\ReturnCode;
use Throwable;
use Webman\Exception\ExceptionHandler;
use Webman\Http\Request;
use Webman\Http\Response;

class ApiHandler extends ExceptionHandler
{
    public $dontReport = [
        BusinessException::class,
    ];

    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    public function render(Request $request, Throwable $exception) : Response
    {
        return json(['code'=>ReturnCode::INVALID,'msg'=>'系统异常!','trace'=>$exception->getMessage()]);
    }
}
