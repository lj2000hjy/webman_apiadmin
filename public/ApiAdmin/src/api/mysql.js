import axios from '@/libs/api.request'

/**
 * 字段列表
 * @returns {wx.RequestTask | never}
 */
export const getFieldList = (params) => {
    return axios.request({
        url: 'MysqlField/index',
        method: 'post',
        params: params
    })
};

/**
 * 新增字段信息
 * @returns {wx.RequestTask | never}
 */
export const addField = (params) => {
    return axios.request({
        url: 'MysqlField/add',
        method: 'post',
        params: params
    })
};

/**
 * 编辑字段信息
 * @returns {wx.RequestTask | never}
 */
export const editField = (params) => {
    return axios.request({
        url: 'MysqlField/edit',
        method: 'post',
        params: params
    })
};

export const delField = (id) => {
    return axios.request({
        url: 'MysqlField/del',
        method: 'post',
        params: {
            id: id
        }
    })
};

/**
 * 显示/隐藏应用
 * @param status
 * @param id
 * @returns {wx.RequestTask | never}
 */
export const changeStatus = (status, id) => {
    return axios.request({
        url: 'MysqlField/changeStatus',
        method: 'post',
        params: {
            status: status,
            id: id
        }
    })
};

